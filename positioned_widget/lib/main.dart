import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var statck = new Stack(
      alignment: const FractionalOffset(0.5, 0.8),
      children: [
        CircleAvatar(
          backgroundImage: NetworkImage(
            'https://picsum.photos/200',
          ),
          radius: 100,
        ),
        Positioned(
          top: 10,
          left: 60,
          child: Text('Text 1'),
        ),
        Positioned(
          bottom: 10,
          right: 10,
          child: Text('Text 2'),
        ),
      ],
    );
    return MaterialApp(
        title: 'Positioned Widget',
        home: Scaffold(
          appBar: AppBar(
            title: Text('Positioned Widget'),
          ),
          body: Center(
            child: statck,
          ),
        ));
  }
}
