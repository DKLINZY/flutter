import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Grid View',
      home: Scaffold(
          appBar: AppBar(
            title: Text('Grid View'),
          ),
          body: GridView(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              mainAxisSpacing: 2.0,
              crossAxisSpacing: 2.0,
              childAspectRatio: 0.7,
            ),
            children: <Widget>[
              Image.network(
                'https://picsum.photos/200/300?grayscale',
                fit: BoxFit.cover,
              ),
              Image.network(
                'https://picsum.photos/200/300?blur',
                fit: BoxFit.cover,
              ),
              Image.network(
                'https://picsum.photos/200/300?blur=2',
                fit: BoxFit.cover,
              ),
              Image.network(
                'https://picsum.photos/200/300?blur=3',
                fit: BoxFit.cover,
              ),
              Image.network(
                'https://picsum.photos/200/300?blur=4',
                fit: BoxFit.cover,
              ),
              Image.network(
                'https://picsum.photos/200/300?blur=5',
                fit: BoxFit.cover,
              ),
              Image.network(
                'https://picsum.photos/200/300?blur=6',
                fit: BoxFit.cover,
              ),
              Image.network(
                'https://picsum.photos/200/300?blur=7',
                fit: BoxFit.cover,
              ),
              Image.network(
                'https://picsum.photos/200/300?blur=8',
                fit: BoxFit.cover,
              ),
              Image.network(
                'https://picsum.photos/200/300?blur=9',
                fit: BoxFit.cover,
              ),
              Image.network(
                'https://picsum.photos/200/300?blur=10',
                fit: BoxFit.cover,
              ),
            ],
          )),
    );
  }
}
