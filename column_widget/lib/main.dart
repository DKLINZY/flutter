import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Column Widget',
        home: Scaffold(
          appBar: AppBar(
            title: Text('Column Widget'),
          ),
          body: Center(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Column 1'),
              Expanded(child: Text('Column 2 Column 2')),
              Text('Column 3'),
            ],
          )),
        ));
  }
}
