import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var card = Card(
      child: Column(
        children: [
          ListTile(
            title: Text(
              'List 1',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            subtitle: Text('List 1 - Content'),
            leading: Icon(
              Icons.account_box,
              color: Colors.lightBlue,
            ),
          ),
          Divider(),
          ListTile(
            title: Text(
              'List 2',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            subtitle: Text('List 2 - Content'),
            leading: Icon(
              Icons.account_box,
              color: Colors.lightBlue,
            ),
          ),
          Divider(),
          ListTile(
            title: Text(
              'List 3',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            subtitle: Text('List 3 - Content'),
            leading: Icon(
              Icons.account_box,
              color: Colors.lightBlue,
            ),
          ),
        ],
      ),
    );
    return MaterialApp(
        title: 'Card Widget',
        home: Scaffold(
          appBar: AppBar(
            title: Text('Card Widget'),
          ),
          body: Center(
            child: card,
          ),
        ));
  }
}
