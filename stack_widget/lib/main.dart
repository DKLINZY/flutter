import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var statck = new Stack(
      alignment: const FractionalOffset(0.5, 0.8),
      children: [
        CircleAvatar(
          backgroundImage: NetworkImage(
            'https://picsum.photos/200',
          ),
          radius: 100,
        ),
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          padding: EdgeInsets.all(5),
          child: Text('Stack Widget'),
        )
      ],
    );
    return MaterialApp(
        title: 'Stack Widget',
        home: Scaffold(
          appBar: AppBar(
            title: Text('Stack Widget'),
          ),
          body: Center(
            child: statck,
          ),
        ));
  }
}
