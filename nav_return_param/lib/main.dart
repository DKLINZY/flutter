import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
      title: 'Navigator Return Parameter',
      home: FirstPage(),
    ));

class FirstPage extends StatelessWidget {
  const FirstPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('First Page'),
      ),
      body: Center(
        child: RouteButton(),
      ),
    );
  }
}

class RouteButton extends StatelessWidget {
  const RouteButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: () {
          _navigateToNextPage(context);
        },
        child: Text('Next Page'));
  }

  _navigateToNextPage(BuildContext context) async {
    final result = await Navigator.push(
        context, MaterialPageRoute(builder: (context) => SecondPage()));
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text('${result}')));
  }
}

class SecondPage extends StatelessWidget {
  const SecondPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Second Page'),
      ),
      body: Center(
        child: Column(
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.pop(context, 'Item 1');
              },
              child: Text('Item 1'),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.pop(context, 'Item 2');
              },
              child: Text('Item 2'),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.pop(context, 'Item 3');
              },
              child: Text('Item 3'),
            ),
          ],
        ),
      ),
    );
  }
}
