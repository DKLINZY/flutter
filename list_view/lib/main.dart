import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'List View',
      home: Scaffold(
        appBar: AppBar(
          title: Text('List View'),
        ),
        // body: ListViewVertical(),
        body: Center(
          child: Container(
            height: 200.0,
            child: ListViewHorizontal(),
          ),
        ),
      ),
    );
  }
}

class ListViewVertical extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        ListTile(
          leading: Icon(Icons.android),
          title: Text('android'),
        ),
        ListTile(
          leading: Icon(Icons.phone),
          title: Text('phone'),
        ),
        ListTile(
          leading: Icon(Icons.flutter_dash),
          title: Text('flutter_dash'),
        ),
        Image.network(
          'https://flutter.dev/assets/images/homepage/carousel/slide_1-bg.jpg',
        ),
        Image.network(
          'https://flutter.dev/assets/images/homepage/carousel/slide_2-bg.jpg',
        ),
        Image.network(
          'https://flutter.dev/assets/images/homepage/carousel/slide_3-bg.jpg',
        ),
        Image.network(
          'https://flutter.dev/assets/images/homepage/carousel/slide_4-bg.jpg',
        ),
      ],
    );
  }
}

class ListViewHorizontal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      scrollDirection: Axis.horizontal,
      children: [
        Container(
          width: 180.0,
          color: Colors.lightBlue,
        ),
        Container(
          width: 180.0,
          color: Colors.amber,
        ),
        Container(
          width: 180.0,
          color: Colors.deepOrange,
        ),
        Container(
          width: 180.0,
          color: Colors.deepPurpleAccent,
        ),
      ],
    );
  }
}
