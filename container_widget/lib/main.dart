import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Container Widget',
      home: Scaffold(
        body: Center(
          child: Container(
            child: Text(
              'Container Widget',
              style: TextStyle(fontSize: 40.0),
            ),
            // topCenter topLeft topRight
            // center centerLeft centerRight
            // bottomCenter bottomLeft bottomRight
            alignment: Alignment.topLeft,
            width: 500.0,
            height: 400.0,
            // color: Colors.lightBlue,
            // all fromLTRB
            padding: const EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 0.0),
            margin: const EdgeInsets.all(10.0),
            decoration: BoxDecoration(
              gradient: const LinearGradient(colors: [
                Colors.lightBlue,
                Colors.greenAccent,
                Colors.purple
              ]),
              border: Border.all(
                width: 5.0,
                color: Colors.red,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
