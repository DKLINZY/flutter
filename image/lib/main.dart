import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Image',
      home: Scaffold(
        body: Center(
          child: Container(
            child: Image.network(
              'https://file.deroraoraoraek.moe/hexo/favicon/favicon.png',
              // contain fill fitWidth fitHeight cover scaleDown
              fit: BoxFit.scaleDown,
              // color: Colors.greenAccent,
              // colorBlendMode: BlendMode.modulate,
              // noRepeat repeat repeatX repeatY
              repeat: ImageRepeat.repeat,
            ),
            width: 300.0,
            height: 200.0,
            color: Colors.lightBlue,
          ),
        ),
      ),
    );
  }
}
