import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Text Widget',
      home: Scaffold(
        body: Center(
          child: Text(
            'Hello Text Widget! Hello Text Widget! Hello Text Widget! Hello Text Widget! Hello Text Widget! Hello Text Widget! ',
            textAlign: TextAlign.center, //left center right start end
            maxLines: 1,
            overflow: TextOverflow.ellipsis, //clip ellipsis fade
            style: TextStyle(
              fontSize: 25.0,
              color: Color.fromARGB(255, 255, 125, 125),
              decoration: TextDecoration.underline,
              decorationStyle: TextDecorationStyle.solid,
            ),
          ),
        ),
      ),
    );
  }
}
